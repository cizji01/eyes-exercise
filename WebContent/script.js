	var sourceIndex = 0; // sets which API from sourceAPI array is used
	var quoteId="#joke"; //div ID
	var minFontSize=8; //px
	var maxFontSize=120; //px
	var is3dBckgOn=true; //jokeContainer

$(document).ready ( function(){ 
	var joke = document.createElement("div");
	joke.id = "joke";
	joke.class = "joke";
	joke.style = null;
	var parent = document.getElementById('jokeContainer');
	parent.appendChild(joke);
}
);
	
var sourceAPI = [
	{name:'GeekJoke', url:'https://geek-jokes.sameerkumar.website/api', method:'GET', returnProperty:'quoteText'},
	{name:'Forismatic', url:'http://api.forismatic.com/api/1.0/?method=getQuote&lang=en&format=jsonp&jsonp=?', method:'getJSON', returnProperty:'json'},	
	{name:'TheySaidSo', url:'https://theysaidso.com/api/', method:'GET'},
]

const setIndex = function (index) {
	this.sourceIndex=index;
}


var nextQuote = function (ApiName, url, tagId) {
	
	var jokeParent = document.getElementById('joke').parentNode;
	var joke = document.getElementById('joke');
	jokeParent.removeChild(joke);
	
	joke = document.createElement("div");
	joke.id = "joke";
	joke.class = "joke";
	joke.style = null;
	jokeParent.appendChild(joke);
		
	$(tagId).html(formatTextWrap(retrieveQuoteText(ApiName,url,tagId),20));
	$("#minFontSizeValue").html(minFontSize);
	$("#maxFontSizeValue").html(maxFontSize);

}

var set3dBackground = function () {
	
	//var joke = document.getElementById('joke');

	if (is3dBckgOn) {
		$("#jokeContainer").css('background-image','url()');
		$("#joke").css('color','#FFFFFF');
		is3dBckgOn = false;
	}
	else {
		$("#jokeContainer").css('background-image','url(\'3d_image.jpg\')');
		$("#joke").css('color','#000000');
		is3dBckgOn = true;
	}
}

var retrieveQuoteText = function (ApiName, url,tagId) {
	var quoteText ="";
	switch (ApiName) {
	
	case 'GeekJoke': 
		$.ajaxSetup({async: false});			
		$.get(url,function(data) {quoteText = data;});		
		$.ajaxSetup({async: true});
		break;
		
	case 'Forismatic':
		$.ajaxSetup({async: false});
		$.getJSON(url, function(data) {$(tagId).html(formatTextWrap(data.quoteText,20)+'<br />'+data.quoteAuthor);});
		$.ajaxSetup({async: true});
		break;
	default:
		break;
	}
	return quoteText;
}

const formatTextWrap = (text, maxLineLength) => {
	  const words = text.replace(/[\r\n]+/g, ' ').split(' ');
	  let lineLength = 0;
	  
	  // use functional reduce, instead of for loop 
	  return words.reduce((result, word) => {
	    if (lineLength + word.length >= maxLineLength) {
	      lineLength = word.length;
	      return result + `<br>${word}`; // don't add spaces upfront
	    } else {
	      lineLength += word.length + (result ? 1 : 0);
	      return result ? result + ` ${word}` : `${word}`; // add space only when needed
	    }
	  }, '');
	}
